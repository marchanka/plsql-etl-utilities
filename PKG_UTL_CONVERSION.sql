alter session set plsql_code_type='NATIVE';

/******************************************************************************/
/************************* PACKAGE DEFINITION *********************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE PKG_UTL_CONVERSION AS
   /** =========================================================================
     Name.................: PKG_UTL_CONVERSION
     Content..............: Package for any conversions
     Author...............: Andrey.Marchenko
     Date.................: 24.07.2017
   ========================================================================= **/

   FUNCTION fnc_utl_utc_to_cet (in_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE PARALLEL_ENABLE;

   FUNCTION fnc_utl_cet_to_utc (in_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE PARALLEL_ENABLE;

   FUNCTION fnc_utl_utime_to_date (in_utime in number default 0) RETURN DATE;
   
   FUNCTION fnc_utl_timestamp_diff_sec (in_timestamp1 in timestamp default current_timestamp,
                                        in_timestamp2 in timestamp default current_timestamp) RETURN NUMBER PARALLEL_ENABLE;

   FUNCTION fnc_utl_ip_to_number (in_ip in varchar2 default '0.0.0.0') RETURN NUMBER PARALLEL_ENABLE;
   
   FUNCTION fnc_utl_str_to_sha1 (in_str in varchar2 default 'N/A') RETURN VARCHAR2 PARALLEL_ENABLE;

END;
/


/******************************************************************************/
/************************* PACKAGE BODY ***************************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE BODY PKG_UTL_CONVERSION AS


/******************************************************************************/
FUNCTION fnc_utl_utc_to_cet (in_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE
IS
BEGIN
  -- return
  RETURN TO_DATE(TO_CHAR(FROM_TZ(CAST(in_date AS TIMESTAMP), 'Etc/Greenwich') AT TIME ZONE 'Europe/Berlin', 'yyyy.mm.dd hh24:mi:ss'),'yyyy.mm.dd hh24:mi:ss');

END fnc_utl_utc_to_cet;


/******************************************************************************/
FUNCTION fnc_utl_cet_to_utc (in_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE
IS
BEGIN
  -- return
  RETURN TO_DATE(TO_CHAR(FROM_TZ(CAST(in_date AS TIMESTAMP), 'Europe/Berlin') AT TIME ZONE 'Etc/Greenwich', 'yyyy.mm.dd hh24:mi:ss'),'yyyy.mm.dd hh24:mi:ss');

END fnc_utl_cet_to_utc;


/******************************************************************************/
FUNCTION fnc_utl_utime_to_date (in_utime in number default 0) RETURN DATE
IS
BEGIN
  -- return
  RETURN TO_DATE('1970.01.01 00:00:00','yyyy.mm.dd hh24:mi:ss') + (1/24/60/60 * in_utime);

END fnc_utl_utime_to_date;


/******************************************************************************/
FUNCTION fnc_utl_timestamp_diff_sec (in_timestamp1 in timestamp default current_timestamp,
                                     in_timestamp2 in timestamp default current_timestamp) RETURN NUMBER
IS
BEGIN
  -- return
  RETURN ABS(
			EXTRACT(SECOND FROM in_timestamp1-in_timestamp2) +
			EXTRACT(MINUTE FROM in_timestamp1-in_timestamp2) * 60 +
			EXTRACT(HOUR   FROM in_timestamp1-in_timestamp2) * 60 * 60 +
			EXTRACT(DAY    FROM in_timestamp1-in_timestamp2) * 24 * 60 * 60
		);

END fnc_utl_timestamp_diff_sec;


/******************************************************************************/
FUNCTION fnc_utl_ip_to_number (in_ip in varchar2 default '0.0.0.0') RETURN NUMBER
IS
v_ip_nr DECIMAL(38,0) := 0;
BEGIN
  -- define ip version
  -- full verification: LENGTH(in_ip) >= 7 AND LENGTH(in_ip) <= 15 AND INSTR(in_ip, '.') > 0
  IF (INSTR(in_ip, '.') > 0) -- IPV4
  THEN
      v_ip_nr := 
      TO_NUMBER(NVL(
      SUBSTR( in_ip, 1, 
        INSTR(in_ip, '.', 1)-1
      ), '0')) * POWER(256, 3) + -- 1th
      TO_NUMBER(NVL(
      SUBSTR( in_ip, INSTR(in_ip, '.', 1)+1,
        ( INSTR(in_ip, '.', INSTR(in_ip, '.', 1, 2)) - INSTR(in_ip, '.', 1)-1 ) 
      ), '0')) * POWER(256, 2) +    -- 2nd
      TO_NUMBER(NVL(
      SUBSTR( in_ip, INSTR(in_ip, '.', 1, 2)+1,
        ( INSTR(in_ip, '.', INSTR(in_ip, '.', 1, 3)) - INSTR(in_ip, '.', 1, 2)-1 )
      ), '0')) * POWER(256, 1)  +     -- 3th
      TO_NUMBER(NVL(
      SUBSTR( in_ip, INSTR(in_ip, '.', 1, 3)+1,
        ( LENGTH(in_ip) - INSTR(in_ip, '.', 1, 2) )
      ), '0')) * POWER(256, 0)          -- 4th
      ;
  -- full verification: LENGTH(in_ip) > 15 AND LENGTH(in_ip) <= 39 AND INSTR(in_ip, ':') > 0
  ELSIF (INSTR(in_ip, ':') > 0) -- IPV6
  THEN
      v_ip_nr := 
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 1) = 0 THEN '0' ELSE 
      SUBSTR( in_ip, 1,
        INSTR(in_ip, ':', 1)-1
      ) END, '0'), 'XXXX') * POWER(65536, 7) + -- 1th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 2) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 2)) - INSTR(in_ip, ':', 1)-1 ) 
      ) END, '0'), 'XXXX') * POWER(65536, 6) +    -- 2nd
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 3) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 2)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 3)) - INSTR(in_ip, ':', 1, 2)-1 )
      ) END, '0'), 'XXXX') * POWER(65536, 5) +    -- 3th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 4) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 3)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 4)) - INSTR(in_ip, ':', 1, 3)-1 )
      ) END, '0'), 'XXXX') * POWER(65536, 4) +    -- 4th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 5) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 4)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 5)) - INSTR(in_ip, ':', 1, 4)-1 )
      ) END, '0'), 'XXXX') * POWER(65536, 3) +    -- 5th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 6) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 5)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 6)) - INSTR(in_ip, ':', 1, 5)-1 )
      ) END, '0'), 'XXXX') * POWER(65536, 2) +    -- 6th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 7) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 6)+1,
        ( INSTR(in_ip, ':', INSTR(in_ip, ':', 1, 7)) - INSTR(in_ip, ':', 1, 6)-1 )
      ) END, '0'), 'XXXX') * POWER(65536, 1) +    -- 7th
      
      TO_NUMBER(NVL( CASE WHEN INSTR(in_ip, ':', 1, 7) = 0 THEN '0' ELSE
      SUBSTR( in_ip, INSTR(in_ip, ':', 1, 7)+1,
        ( LENGTH(in_ip) - INSTR(in_ip, ':', 1, 7) )
      ) END, '0'), 'XXXX') * POWER(65536, 0)    -- 8th
      ;
  ELSE v_ip_nr := 0;
  END IF;
  
  RETURN v_ip_nr;

END fnc_utl_ip_to_number;


/******************************************************************************/
FUNCTION fnc_utl_str_to_sha1 (in_str in varchar2 default 'N/A') RETURN VARCHAR2
IS
BEGIN
  -- return
  RETURN LOWER( TO_CHAR( RAWTOHEX( 
               SYS.DBMS_CRYPTO.HASH(
                 UTL_I18N.STRING_TO_RAW(in_str, 'AL32UTF8'), SYS.DBMS_CRYPTO.HASH_SH1
               )
         ) ) );

END fnc_utl_str_to_sha1;


/******************************************************************************/

END PKG_UTL_CONVERSION;
/

alter session set plsql_code_type='INTERPRETED';

