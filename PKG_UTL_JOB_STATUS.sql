
/******************************************************************************/
/************************* PACKAGE DEFINITION *********************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE PKG_UTL_JOB_STATUS AS
   /** =========================================================================
     Name.................: PKG_UTL_JOB_STATUS
     Content..............: Job Control Package
     Author...............: Andrey.Marchenko
     Date.................: 17.05.2017

   ========================================================================= **/

   FUNCTION fnc_utl_new_sid (in_job_name in char default 'N/A',
                             in_force_restart in char default 'N') RETURN NUMBER;

   FUNCTION fnc_utl_put_log (in_sid in number default -1,
                             in_job_name in char default 'N/A',
                             in_step_name in char default 'N/A',
                             in_step_descr in char default 'N/A') RETURN NUMBER;

   FUNCTION fnc_utl_get_last_success_date (in_job_name in char default 'N/A',
                                           in_default_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE;

   FUNCTION fnc_utl_get_latest_date (in_job_name in char default 'N/A',
                                     in_default_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE;
                             
END;
/


/******************************************************************************/
/************************* PACKAGE BODY ***************************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE BODY PKG_UTL_JOB_STATUS AS


/******************************************************************************/
FUNCTION fnc_utl_new_sid (in_job_name in char default 'N/A',
                          in_force_restart in char default 'N') RETURN NUMBER
AS
   v_running NUMBER := 0;
   v_return  NUMBER := 0;
   v_id      NUMBER := 0;
BEGIN
  -- get new sid
  v_id := s_sid.nextval;
  
  -- job is finished logic
  SELECT SUM (
  CASE WHEN start_job.job_name IS NOT NULL AND end_job.job_name IS NULL -- existed job finished
            AND NOT ( 'Y' = in_force_restart ) -- force restart PARAMETER
       THEN 1 ELSE 0
  END ) AS IS_RUNNING
  INTO v_running
  FROM
    ( SELECT 1 AS dummy FROM utl_job_status WHERE sid = -1) d_job
  LEFT OUTER JOIN
    ( SELECT job_name, sid, 1 AS dummy
      FROM utl_job_status
      WHERE job_name = in_job_name -- job name PARAMETER
            AND step_name = 'JOB_START'
      GROUP BY job_name, sid
    ) start_job -- starts
  ON d_job.dummy = start_job.dummy
  LEFT OUTER JOIN
    ( SELECT job_name, sid
      FROM utl_job_status
      WHERE job_name = in_job_name
            AND step_name in ('JOB_END', 'JOB_ERROR') -- stop status
      GROUP BY job_name, sid
    ) end_job -- ends
  ON start_job.job_name = end_job.job_name
     AND start_job.sid = end_job.sid;
  
  -- job is finished or new one
  IF (v_running = 0) THEN
     -- return new Session ID
     RETURN v_id;
  ELSE
     RETURN -1;
  END IF;
  
  -- when error
  EXCEPTION
      WHEN OTHERS THEN
         ROLLBACK; RAISE;
         
END fnc_utl_new_sid;


/******************************************************************************/
FUNCTION fnc_utl_put_log (in_sid in number default -1,
                          in_job_name in char default 'N/A',
                          in_step_name in char default 'N/A',
                          in_step_descr in char default 'N/A') RETURN NUMBER
AS
   v_id NUMBER := 0;
BEGIN

  v_id := s_utl_job_status_id.nextval;

  -- write log
  INSERT /*+ APPEND */ INTO utl_job_status (utl_job_status_id, sid, job_name, step_name, step_descr)
  VALUES (v_id, in_sid, in_job_name, in_step_name, in_step_descr);
  COMMIT;
  -- job is finished or new one
  IF (v_id > 0) THEN
     -- return new Session ID
     RETURN v_id;
  ELSE
     RETURN -1;
  END IF;
  
  -- when error
  EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK; RAISE;
         
END fnc_utl_put_log;


/******************************************************************************/
FUNCTION fnc_utl_get_last_success_date (in_job_name in char default 'N/A',
                                        in_default_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE
AS
   v_d DATE := TRUNC(SYSDATE);
BEGIN

  -- get
  SELECT NVL(MAX(LOG_D), in_default_date) AS LAST_SUCCESS_DATE
  INTO v_d
  FROM utl_job_status
  WHERE 1 = 1
    AND LOG_D < TRUNC(SYSDATE) - 1
    AND step_name = 'JOB_END'
    AND job_name = in_job_name;

  -- return
  RETURN v_d;
  
  -- when error
  EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK; RAISE;
         
END fnc_utl_get_last_success_date;


/******************************************************************************/
FUNCTION fnc_utl_get_latest_date (in_job_name in char default 'N/A',
                                  in_default_date in date default TO_DATE('01.01.1111','DD.MM.YYYY')) RETURN DATE
AS
   v_d DATE := TRUNC(SYSDATE);
BEGIN

  -- get
  SELECT NVL(MAX(LOG_D), in_default_date) AS LATEST_DATE
  INTO v_d
  FROM utl_job_status
  WHERE 1 = 1
    AND step_name = 'JOB_END'
    AND job_name = in_job_name;

  -- return
  RETURN v_d;
  
  -- when error
  EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK; RAISE;
         
END fnc_utl_get_latest_date;


/******************************************************************************/
BEGIN
  DBMS_OUTPUT.ENABLE(4000);

  EXCEPTION
    WHEN OTHERS THEN
      -- Never stop
      DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
      NULL;

END PKG_UTL_JOB_STATUS;
/
