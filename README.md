	--------------------------------------------------------------------------
	Description
	--------------------------------------------------------------------------
    This repository contains an utilities to control and log the extract transform and load procedures written in PLSQL.
    ETL is an queue of SQL statements which execute in a steps.
    Every ETL Job gets an unique execution session identification like SID.
    The predefined status like JOB_START/JOB_END/JOB_ERROR is used to prevent parallel execution of the same ETL.
    Input parameters of different data types could be used for custom control of ETL statements. Like initial/delta load, load with date filter.
    Some examples of ETL will be posted on www.marchenko.de


	UTL_JOB_STATUS.sql - log table for ETL jobs
	V_UTL_JOB_STATUS_LOG.sql - execution report
	PKG_UTL_JOB_STATUS.sql - ETL Utilities itself
	PKG_STG_SRC_ETL_TEMPLATE.sql - sample template for your ETL job
	
	PKG_STG_SRC_TABLENAME. Job Template.
	The global variable gv_job_name is to be defined for every ETL package like 'PKG_STG_SRC_TABLENAME'. Means Package for stage target table TABLENAME from the source SRC
	ETL step is to be described within variable v_step_name. Like DB_SHEMA.TARGET_TABLE.SECOND_SELECT_INSERT
	
	PKG_UTL_JOB_STATUS. ETL utilities. The package supports following input parameter
	in_force_restart:
	  Y - the package will start regardless of the previous state
	  N - the package will start only if the previous state is 'JOB_END' or 'JOB_ERROR'
	in_full_load:
	  Y/N - can be used within ETL logic
	in_load_dt_from, in_load_dt_to:
	  Date - can be used within ETL logic
	in_load_nr_from, in_load_nr_to:
	  Number - can be used within ETL logic
	
	--------------------------------------------------------------------------
	Installation
	--------------------------------------------------------------------------
	
	Create Objects:
	- execute UTL_JOB_STATUS.sql
	- execute V_UTL_JOB_STATUS.sql
	- execute PKG_UTL_JOB_STATUS.sql
	- execute PKG_STG_SRC_TABLENAME.sql
	
	Usage Exsample:
	- execute ETL with default parameter: exec PKG_STG_SRC_TABLENAME.PRC_JOB_START();
    - execute ETL with parameter Full Load: exec PKG_STG_SRC_TABLENAME.PRC_JOB_START('N','Y');
	
	--------------------------------------------------------------------------
	Monitoring
	--------------------------------------------------------------------------
	
	See ETL Execution Report:
	- SELECT * FROM V_UTL_JOB_STATUS_LOG ORDER BY LOG_DT DESC
	
    "SID" "LOG_DT" "SOURCE" "LAYER" "JOB_NAME"              "STATUS" "STEP_LOG"            "STEP_CNT" "AFFECTED_ROWS" "JOB_START_DT"  "JOB_END_DT"
    51034 06.03.19 "SRC"    "STG"   "PKG_STG_SRC_TABLENAME" "OK"     "JOB_END : Finished." 4          4               06.03.19 20:04  06.03.19 20:04
    51033 06.03.19 "SRC"    "STG"   "PKG_STG_SRC_TABLENAME" "OK"     "JOB_END : Finished." 4          4               06.03.19 20:04  06.03.19 20:04

	See ETL Status:
	- SELECT * FROM UTL_JOB_STATUS ORDER BY LOG_DT DESC
	
	"UTL_JOB_STATUS_ID" "SID" "LOG_DT" "LOG_D"  "JOB_NAME"              "STEP_NAME"                                  "STEP_DESCR"
    248813              51034 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "JOB_END"                                    "Finished."
    248812              51034 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "DB_SHEMA.TARGET_TABLE.SECOND_SELECT_INSERT" "Affected Rows: *** 2 ***"
    248811              51034 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "DB_SHEMA.TARGET_TABLE.FIRST_SELECT_INSERT"  "Affected Rows: *** 2 ***"
    248810              51034 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "JOB_START"                                  "SID:51034 force_restart:N full_load:Y load_dt_from:01.01.11 load_dt_to:31.12.99 load_nr_from:0 load_nr_to:0"
    248809              51033 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "JOB_END"                                    "Finished."
    248808              51033 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "DB_SHEMA.TARGET_TABLE.SECOND_SELECT_INSERT" "Affected Rows: *** 2 ***"
    248807              51033 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "DB_SHEMA.TARGET_TABLE.FIRST_SELECT_INSERT"  "Affected Rows: *** 2 ***"
    248806              51033 06.03.19 06.03.19 "PKG_STG_SRC_TABLENAME" "JOB_START"                                  "SID:51033 force_restart:N full_load:N load_dt_from:01.01.11 load_dt_to:31.12.99 load_nr_from:0 load_nr_to:0"
	
	See Tablespace status:
	- SELECT * FROM V_UTL_TABLESPACE_STATUS
	
    "LAYER" "TS_NAME"         "SIZE_MB"   "FREE_MB" "USAGE_PCT" "STATUS"
    "STG"    "STG_PROJ1_DATA" "67640"     "55790"   "17,52"     "OK"
    "STG"    "STG_PROJ2_DATA" "122740"    "3224"    "97,37"     "ALERT"
    "STG"    "STG_PROJ2_DATA" "1433600"   "80221"   "94,4"      "WARNING"

	See Sanity Check status:
	- SELECT * FROM V_UTL_JOB_STATUS_SANITY_QC

	

