
CREATE OR REPLACE FORCE VIEW V_UTL_JOB_STATUS_SANITY_QC
AS
WITH TESTS AS (
-- Project job and step from status table
SELECT 'PCK_FCT_SRC_DATA' AS JOB_NAME, 'DM_STAR.FCT_SRC_DATA_HH' AS STEP_NAME FROM DUAL
UNION ALL
SELECT 'PCK_STG_QUL_TABLE' AS JOB_NAME, 'STG_QUL.SO_TABLE_CLS' AS STEP_NAME FROM DUAL
-- and other projects with UNION ALL
-- ...
)
SELECT
JOB_NAME, STEP_NAME, SOURCE, LAYER, EMPTY_CNT, ROW_CNT, CHECK_DAY_CNT, RUN_DAY_CNT, MAX_DAY_CNT,
CASE
     WHEN RUN_DAY_CNT < CHECK_DAY_CNT THEN 'ERROR'
     WHEN (EMPTY_CNT > EMPTY_CNT + RUN_DAY_CNT) OR (EMPTY_CNT > 0 AND ROW_CNT*CHECK_DAY_CNT/ROW_CNT <= CHECK_DAY_CNT) THEN 'WARNING'
     ELSE 'OK' END AS STATUS,
CASE
     WHEN RUN_DAY_CNT < CHECK_DAY_CNT THEN 'Job not started ' || (CHECK_DAY_CNT - RUN_DAY_CNT) || ' times' -- days not loaded, job not started
     WHEN (EMPTY_CNT > EMPTY_CNT + RUN_DAY_CNT) OR (EMPTY_CNT > 0  AND ROW_CNT*CHECK_DAY_CNT/ROW_CNT <= CHECK_DAY_CNT) THEN 'Job started but no data loaded ' || EMPTY_CNT || ' times' -- job started but no data loaded
     ELSE 'No problem' END AS DESCR
FROM (
SELECT 
  TST.JOB_NAME,
  TST.STEP_NAME,
  NVL(SUBSTR(TST.JOB_NAME, INSTR(TST.JOB_NAME, '_', 1, 2)+1, INSTR(TST.JOB_NAME, '_', 1, 3)-INSTR(TST.JOB_NAME, '_', 1, 2)-1), 'N/A') AS SOURCE,
  NVL(SUBSTR(TST.JOB_NAME, INSTR(TST.JOB_NAME, '_', 1, 1)+1, INSTR(TST.JOB_NAME, '_', 1, 2)-INSTR(TST.JOB_NAME, '_', 1, 1)-1), 'N/A') AS LAYER,
  SUM( DECODE(TRIM(UST.STEP_DESCR), 'Affected Rows: *** 0 ***', 1, 0) )                                                               AS EMPTY_CNT,
  SUM( 1 )                                                                                                                            AS ROW_CNT,
  5                                                                                                                                   AS CHECK_DAY_CNT,
  COUNT( DISTINCT UST.LOG_D )                                                                                                         AS RUN_DAY_CNT,
  1 + MAX( NVL(UST.LOG_D, TRUNC(SYSDATE)) ) - MIN( NVL(UST.LOG_D,TRUNC(SYSDATE - 5) + 1) )                                            AS MAX_DAY_CNT
FROM UTL_JOB_STATUS UST,
     TESTS TST
WHERE 1 = 1
AND UST.LOG_D (+) >= (TRUNC(SYSDATE) - 4) -- track last 5 days
AND TST.JOB_NAME = UST.JOB_NAME (+)
AND TST.STEP_NAME = UST.STEP_NAME (+)
GROUP BY TST.JOB_NAME,
         TST.STEP_NAME
) JST
ORDER BY JST.SOURCE, JST.LAYER, JST.JOB_NAME, JST.STEP_NAME;

---- COMMENTS
COMMENT ON TABLE V_UTL_JOB_STATUS_SANITY_QC IS 'Content: Quality Check of Package Execution. WARNING means the package started but no data loaded, ERROR means package not started.';

